title: common-lisp.net News

---

# Latest Common-Lisp.net news

### 2020-04-05

Mailing list archive clean-up: Since the inception of common-lisp.net,
hosting of mailing lists has been part of our service offering. For
a long time, two or even three mailing lists were created with every
new project. Many of those were never used. Today, we cleared out all
mailing list archives which didn't have any messages in them *and*
which are not in active operation.

This cleanup should allow more effective browsing of the archives as
571 mailing lists without content are no longer part of the archive
listing.

If you think we cleared out an archive in error, please let us know
so we can restore.

### 2019-12-27 *ABCL fundraiser!*

The [Common Lisp Foundation](https://cl-foundation.org/) hosts an
[appreciation fundraiser](https://payments.common-lisp.net/abcl) for the [ABCL project](https://abcl.org/).

An appreciation fundraiser serves as a 'thank you' to those who have invested
their (free) time in the project. It isn't intended as a 'bounty' for new
functionality.


### 2019-06-01

The Subversion repository maintenance window completed ahead
of schedule at 18:00 on June 1st instead of taking the better
part of the weekend. During this maintenance window, Subversion
repositories were upgraded to the latest standard (1.9). Some
repositories were stored in backward compatible formats as far
back as 1.1. With the migration, repository access should
benefit from performance work done in the server component.
Other features that become available with this maintenance
include the storage of merge-tracking information.

Thank you for your patience!

### 2019-05-19

The site has always been running its own Trac setup, because
we were one of the early Trac adopters. While running our own
packages, we've also been lenient to install plugins. Due to
improved support in both Trac as well as Debian packages, we
were able to move to a full-standard setup.

These days, many of those plugins have been integrated into Trac
or come pre-packaged from Debian. Others have lost their usefulness
(OpenID support falls into this category as nobody seems to be
supporting that anymore...).

For the common-lisp.net team this means we've been able to
maintain both our liberal support for user-desired functionality
of our Trac users as well as simplifying maintenance of our hosting
infrastructure.

Thanks to the Trac team for their continued efforts!

### 2019-04-28

On Sunday, April 28th, the common-lisp.net hosting infrastructure experienced
an outage of approximately 5 hours when we encountered a unexpected
difficulty in upgrading our services.  As you know, we take our responsiblity
towards the integrity of the data and services associated with common-lisp.net
seriously.  Part of that responsiblity is owning up to our mistakes, so here’s
our deeper explanation of how we inadvertently impinged on the integrity of the
main host file system.

While work was in progress on changes to set up support for better and faster
restore from backup, if such was required, a small change was required on the
main BTRFS filesystem: moving a snapshot from one place to another on the file
system. This operation couldn't be performed while the system was online, so
the VM was brought down.

After the VM was down, the main filesystem was mounted on the host system to
perform the operation. After unmounting the filesystem and remounting it to
verify the result, the filesystem at first wouldn't be mounted. After
executing the `btrfs rescue zero-log` command, the filesystem did mount,
but only in read-only mode.

Using a combination of the daily backups and the read-only filesystem,
a new filesystem with the same content been created.

I'm very sorry for the incident in general and having your data at risk in
particular. I haven't found any data-loss.

Erik Hulsmann
common-lisp.net admin

### 2019-02-09 *ASDF fundraiser!*

The [Common Lisp Foundation](https://cl-foundation.org/) hosts an
[appreciation fundraiser](https://payments.common-lisp.net/asdf/) for the [ASDF project](https://common-lisp.net/project/asdf/),
with up to $5,000 __donation doubler__.

An appreciation fundraiser serves as a 'thank you' to those who have invested
their (free) time in the project. It isn't intended as a 'bounty' for new
functionality.


### 2018-11-18

There was a short period of down time today in order to support an
off-line reorganization of our file systems required to simplify and
strengthen our backups and system maintenance tasks.

### 2018-11-09

Our GitLab instance now has a more open account policy: users with a
social login will be able to create an account without approval by
an admin.

In order to support this openness, a more strict login policy has
been put into place, requiring all users to log in using 2FA.

For those users who don't have a smartphone to install a 2FA
app on, [here's a c|net article how to do
2FA anyway](https://www.cnet.com/how-to/how-to-use-two-factor-authentication-without-a-phone/)


### 2018-11-03

There's now infrastructure available to projects to [deploy their project pages
through GitLab](https://common-lisp.net/faq/using-gitlab-deploy-project-pages).

This infrastructure is now being used to [migrate those project pages
to GitLab Pages
deployment](https://mailman.common-lisp.net/pipermail/clo-devel/2018-November/001294.html)
for which there are no OS-level users anymore to update them.


### 2018-10-26

As announced on October 12, empty projects have been removed (archived) from the
system. After further cleaning, the updated list consists of these 23 projects:

* cl-amqp
* cl-cactus-kev
* cl-captcha
* cl-gdbm
* clint
* cl-lazy-list
* cl-lexer
* clotnet
* cl-player
* cl-rundown
* cltl3
* decl
* defmud
* docutrack
* israel-lisp
* lisp-interface-library
* mk-defsystem
* movies
* objective-cl
* phorplay
* same
* trivial-features
* umpa-lumpa


### 2018-10-12 *Intent to remove unused projects*

As per [the announcement](https://mailman.common-lisp.net/pipermail/clo-devel/2018-October/001253.html),
the site admins plan to remove the following list of projects which have been determined to neither
have a project page nor a code repository. Please contact the site admins before October 26, 2018 to
cancel project cleanup in case your project is wrongfully listed.

* arxana
* asdf-install
* asdf-packaging
* boston-lisp
* chemboy
* cl-amqp
* cl-buchberger
* cl-cactus-kev
* cl-captcha
* cl-clickatell
* cl-gdbm
* cl-gtk2
* clint
* cl-lazy-list
* cl-lexer
* cl-match
* cl-ode
* clotnet
* cl-peg
* cl-player
* cl-rundown
* cltl3
* cl-yacc-ebnf
* decl
* defmud
* docutrack
* innen
* israel-lisp
* jess-parse
* lisp-interface-library
* mk-defsystem
* movies
* objective-cl
* phorplay
* same
* sqlisp
* tioga
* trivial-features
* trivial-shell
* ucs-sort
* umpa-lumpa
* xml-psychiatrist

### 2018-10-12

Today I found some time to enable GitLab's 'reply by e-mail' functionality.

When you receive a notification e-mail from our GitLab instance, you can now send a reply to that notification. If the notification was from an issue being updated, then your reply will update the issue with a follow-up commont.

### 2018-10-10

A new version of Common-Lisp.net has been launched!

Among other things, the new site features:

- A site generator implemented in Common Lisp.
- A gitlab pipeline for automatic deployment.
- A new responsive design based on Bootstrap and new content.

### 2018-10-06

As per [the announcement in August](https://mailman.common-lisp.net/pipermail/clo-devel/2018-August/001221.html),
the list of users with O/S level access has been cleaned up: those accounts which haven't been accessed since
2013 (and for which the owner didn't object to removal), have been removed. Home directories will be destructed
to comply with GDPR regulations.

If you once had an account and you need your access restored, please contact our admins.

### 2018-02-17

The Darcs (a distributed version control system before Git) repositories listed in our darcsweb instance
have been converted to [our GitLab instance](https://gitlab.common-lisp.net/). All use of Darcs on our site
has been marked deprecated for a long time and we're happy to have found a way forward, keeping the
repository content available to more modern VCSes.

### 2017-07-15

Our server was upgraded to Stretch as part of the regular life cycle management. See [our announcement](https://mailman.common-lisp.net/pipermail/clo-devel/2017-July/001166.html).

### 2015-05-25

Today the system has been declared "CVS clean"! All remnants of CVS have been removed.

### 2015-05-23

On Friday, the system had performance problems due to slow I/O. Checking the S.M.A.R.T. parameters of the disks didn't show anything unusual. Today, the disk system reports running in degraded mode, so we ordered the failing disk to be replaced, causing only minor downtime. The disk array has been rebuilt and it's smooth sailing from here again!

### 2015-05-22

GitLab upgraded to [GitLab 7.11.1.](https://gitlab.com/gitlab-org/gitlab-ce/blob/v7.11.1/CHANGELOG)

### 2015-05-15

Proposed approach for [Darcs repository migration](https://mailman-test.common-lisp.net/pipermail/clo-devel/2015-May/001051.html) sent out.

### 2015-05-12

The final batch of CVS projects has been migrated. None of the projects opted to move to Subversion, so all of them were migrated to Git+GitLab:

*   gamelib
*   ganelon
*   geometry
*   ginseng
*   glouton
*   gsharp
*   gtk-cffi
*   html-template
*   hyperdoc
*   hyperspec-lookup
*   ieeefp-tests
*   imago
*   iso8601-date
*   jnil
*   lambda-gtk
*   lgtk
*   lispfaq
*   lispy
*   lmud
*   log4cl
*   mcclim
*   meta-cvs
*   misc-extensions
*   morphologie
*   movitz
*   nio
*   noctool
*   pal
*   pg
*   phemlock
*   plain-odbc
*   rfc2388
*   rfc2822
*   rjain-utils
*   rucksack
*   sapaclisp
*   sb-simd
*   snmp1
*   spray
*   stamp
*   s-xml
*   s-xml-rpc
*   trivial-freeimage
*   unetwork
*   zip
*   zlib

Now that the CVS repositories have been migrated, we'll move to clean up the cvs services from the underlying box and preparations will start to migrate the Darcs repositories on the system. For progress on the Darcs repository migration, watch the [clo-devel mailing list](https://mailman.common-lisp.net/listinfo/clo-devel).

### 2015-05-03

The next batch of CVS projects has been migrated. None of the projects opted to move to Subversion, so all of them were migrated to Git+GitLab:

*   cl-magick
*   cl-menusystem
*   cl-mp3-parse
*   closure
*   clouchdb
*   cl-plus-ssl
*   cl-ppcre
*   cl-prevalence
*   cl-rope
*   cl-sbml
*   cl-screen
*   cl-selenium
*   cl-semantic
*   cl-smogames
*   cl-smtp
*   cl-snmp
*   cl-soap
*   cl-store
*   cl-syslog
*   cl-telnetd
*   cl-utilities
*   cl-wav-synth
*   cl-who
*   cl-x86-asm
*   cl-xmms
*   cl-xmpp
*   corman-sdl
*   cparse
*   crypticl
*   cxml
*   defdoc
*   defeditor
*   definer
*   defplayer
*   defwm
*   eclipse
*   elephant
*   fetter
*   flexichain
*   fomus

### 2015-04-27

GitLab project fixes issues reported by common-lisp.net admin: [loading of commit data](https://gitlab.com/gitlab-org/gitlab-ce/issues/1478).

### 2015-04-26

The next batch of CVS projects has been migrated. None of the projects opted to move to Subversion, so all of them were migrated to Git+GitLab:

*   cinline
*   cl-blog
*   cl-carbon
*   cl-cli-parser
*   cl-cracklib
*   cl-dbf
*   cldoc
*   cl-enumeration
*   cl-fltk
*   cl-godb
*   cl-gsl
*   climacs
*   clim-desktop
*   cl-interpol
*   cl-ipc
*   cl-jpeg
*   cl-libtai

### 2015-04-22

GitLab has been upgraded to 7.10.0; [one of the tickets we filed with the GitLab project](https://gitlab.com/gitlab-org/gitlab-ce/issues/1408), has been resolved now.

### 2015-04-19

Migration of CVS repositories to Git+GitLab has started. The first batch (10 projects) was converted:

*   anaphora
*   aspiwork-pdm
*   bayescl
*   beep
*   beirc
*   bese
*   cello
*   cells
*   cells-gtk
*   cells-ode

In total there are 120 CVS repositories to convert. Project members receive an announcement mail in advance. CVS repositories which are left behind after conversion to another VC will be skipped and projects which are in such a situation are kindly requested to notify the admins of such situation - or even better: clean it up before the admins run into it. Now is your chance! :-)

### 2015-04-17

[GitLab](https://gitlab.common-lisp.net/) upgraded to [7.9.4](https://about.gitlab.com/2015/04/15/gitlab-7-dot-9-4-released/)

### 2015-04-11

The first projects with CVS repositories have received e-mails about the planned migration according to the [proposed procedure](https://mailman.common-lisp.net/pipermail/clo-devel/2015-April/001035.html). We're going in batches, using aphabetical order, so you may not have received e-mail for your projects yet. Don't dispair, it'll come!

The announcement e-mail requires action _if you want to migrate to Subversion instead of to Git_. If you want to migrate to Git+[GitLab](https://gitlab.common-lisp.net/), there's no need to respond. It's the default.

If you want to declare that your project wants to migrate to Subversion without awaiting your announcement e-mail, please [file a ticket in the site's Trac instance](https://trac.common-lisp.net/clo/newticket).

### 2015-04-08

Due to a variety of reasons, the history of the mailing lists of common-lisp.net had spread over a number of sources:

*   the older system's mailman list manager's history (mbox format)
*   the intermediately used mlmmj list manager's history (spool directory, individual mails)
*   the new system's mailman list manager's history (mbox format)

With only the archives from the last bullet hosted in viewable form, valuable Common Lisp history and coding advice collected over the years has been unavailable for at least 9 months.

Now, this long-overdue task has finally been completed. The mailing list history can be browsed and indexed again from [the online archive](https://mailman.common-lisp.net/pipermail)

### 2015-03-27

Today we enabled project imports from GitHub. Should you want to import your project (which should import both the repository and the issues) into gitlab, please check out [gitlab documentation for the github import workflow](http://doc.gitlab.com/ce/workflow/import_projects_from_github.html).

As part of the effort to enable GitHub imports, we had to do a minor GitLab upgrade. The current hosted GitLab version is now 7.9.1.

Another part of enabling GitHub imports was to allow users to log in on our site using their GitHub credentials. Please note that this will _only_ work for those who already have a valid (i.e. pre-existing and confirmed) GitLab account. If you want to use this option (which is also available for Google accounts), please check the [GitLab omniauth for existing users documentation](http://doc.gitlab.com/ce/integration/omniauth.html#enable-omniauth-for-an-existing-user).

### 2015-03-24

GitLab upgraded to 7.9.0\. GitLab released its newest version (7.9.0) on the Sunday after the Friday we went live with our self-hosted GitLab (7.8.4 -- then latest). The release contains a [large number of fixes and enhancements](https://github.com/gitlabhq/gitlabhq/blob/v7.9.0/CHANGELOG).

### 2015-03-20

Our self-hosted git repository management environment [gitlab.common-lisp.net](https://gitlab.common-lisp.net/) is now live! During the deployment, the system was seeded with 390 users, 434 groups and 210 repositories.

The deployment itself took little over an hour, while we were able to resolve the issues we identified after completion well within the migration timeframe of 4 hours. During this time, most services were fully available. Only SSH has been impacted shortly.

If you experience any issues, please report to the site admins.

We hope you enjoy the new user experience!

### 2015-03-16

Today the date has been [announced](https://mailman.common-lisp.net/pipermail/clo-devel/2015-March/000972.html) for the final migration of all git repositories to GitLab: Friday March 20th!

### 2015-03-08

While testing the seeding scripts for the new GitLab site, a number of e-mails to live users have unintentionally been sent instead of held.

The content of the messages suggests one may be able to log in on the system. When trying so, the system denies access. This is a side effect of the testing setup. In the final migration run, you will receive an account confirmation mail. A mail not distributed in the test-run. Once the account is confirmed, the system will allow changing passwords and logging in.

We're sorry for any inconvenience caused.

### 2015-02-19

The site admins have announced a change in policies for the mailing lists to be in effect as of March 15th, 2015\. Please read [the original announcement e-mail for details](https://mailman.common-lisp.net/pipermail/clo-devel/2015-February/000941.html).

In summary, there are two changes being implemented:

1.  Subject prefix '[<mailing-list>]' won't be added anymore.
2.  Moderation requests won't be held indefinitely anymore, defaulting to a hold period of 90 days (expiring the unmoderated e-mail when unhandled).

The former addresses problems with delivery of DKIM signed mail being sent through mailing lists. Changing the message headers and body may invalidate any prior DKIM signatures, leading to mailing list e-mail looking suspect to large mail receiving providers like Google and Yahoo!.

The later addresses the fact that the moderation queue is growing indefinitely, with most lists set to never expire moderation requests.

### 2015-02-10

The site admins identified [a list of mailing lists for which contact with the list owner has been lost](/orphaned-mailing-lists/) list for your favorite list or your name.

### 2015-02-08

Due to a failed system upgrade, the system experienced extended downtime on Sunday. Our apologies for any inconvenience.

The maintenance was completed successfully around 18.00 UTC and is a step towards easier maintainance for the system and support the development of [new and simplified configuration](#2015_02_06).

### 2015-02-06

Erik Huelsmann -one of the common-lisp.net maintainers- has submitted a proposal for the nearby future of the site. Read [the details in his mail](https://mailman.common-lisp.net/pipermail/clo-devel/2015-February/000934.html) to the [clo-devel mailing list](https://mailman.common-lisp.net/cgi-bin/mailman/listinfo/clo-devel) in the archive. Feedback is very much appreciated.

### 2015-02-01

The (www.)common-lisp.net domain has started using the _https_ protocol for all its URLs. In the weeks to come, the various sub-domains will be switched too. This change has been made in response to user requests. Should you have a request too, please let us know on the [mailing list](http://mailman.common-lisp.net/cgi-bin/mailman/listinfo
                                            /clo-devel)!

### 2015-01-30

The mail system's criteria for accepting mail have been tightened and mail forwarding now uses <abbr title="Sender rewriting scheme">[SRS](http://en.wikipedia.org/wiki/Sender_Rewriting_Scheme)</abbr> when the sender domain specifies an [<abbr title="Sender Policy Framework">SPF</abbr> policy](http://en.wikipedia.org/wiki/Sender_Policy_Framework). The changes address these issues:

*   too many spam mails entering the moderation queues
*   improved reputation of the common-lisp.net domain at Google

Google considers common-lisp.net a Bulk Sender, which means we must abide by [a number of requirements to make sure mail gets accepted](https://support.google.com/mail/answer/81126?hl=en).

Some of the stricter criteria to accept mail on common-lisp.net are:

Mail failing SPF validation is no longer accepted.

Due to the SRS rewriting, these mails would leave common-lisp.net as legitimate mail. By rejecting these mails, we honor the policy stated by the sender domain.  
_Note: We're currently not rejecting soft-failing mails._

Mail failing DKIM validation is no longer accepted

Forwarding mail which fails DKIM validation reflects poorly on common-lisp.net's reputation as a mail host. By rejecting mail failing DKIM validation, we honor the policy stated by the sender domain.

A series of requirements on the host submitting mail

By enforcing a number of requirements on the DNS configuration and protocol conformity of the hosts submitting mail to common-lisp.net, we enforce the same requirements that Google and other mail hosters enforce on us, common-lisp.net can't be used as a hub by spammers to increase legitemacy of the mail flow. Nor can accidental forwarding of spam mail hurt common-lisp.net's reputation with the large mail hosters.

### 2014-09-22

Hello everybody. The site moved to a shiny new server a few moments ago. Everything should work, and if it doesn't, please [let us know.](http://mailman.common-lisp.net/cgi-bin/mailman/listinfo/clo-devel)

You can also help us by [donating](/contribute/).
