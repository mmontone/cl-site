# Common-Lisp.net site generator

## Directory structure

### Content

The site content is divided over the directories `content/`
and `layout`.

The `layout/` directory holds all content
that applies to more than one page (e.g. templates) and
everything that's referenced from such components (e.g.
logo and other images or CSS), or is used to override
such a component (e.g. a template which overrides the
default template). Referred content, such as stylesheets,
scripts and images are located in `layout/static/*`.

The `content/` directory holds the individual content items (pages)
and everything that's referred from that content, but not referred
from layout content. E.g.  the `index.html` page — defining the
site's front page — is located in `content/` as well as any images
that are included on the front page but not included on any template
content. Referred content, such as stylesheets, scripts and images are
located in `content/static/*`

The `content/` directory can contain both `html` files and `lisp`
files. `html` files are processed with the template stuff directly
into the `output/` directory. `lisp` files are expected to have a call
to `cl-site:add-content` as their toplevel form, which takes arguments
of `:headers-plist` and `html-string`. See `content/about.lisp` for a
reference example.

If a file by the same name is stored in both
`layout/static/` and `content/static/`, then the one
in `layout/static/` takes precedence. 


### Generator Source Code

The source code for the site generator is located in the
root of the repository. The `t/` directory contains tests
for the site generator.

### Server Setup Code (optional)

The `servers/` directory contains code for running various CL HTTP
servers to be able to serve the generated content locally (e.g. while
authoring/editing).  It has [its own
README](https://gitlab.common-lisp.net/dcooper/cl-site/blob/master/servers/README.md).


## Dependencies

* CL-MUSTACHE for HTML templating
* CL-YAML for YAML "header" parsing
* CLOSER-MOP (because...?)
* CL-WHO for LHTML processing
* MARKDOWN.CL for Markdown processing

(these are all called out in the cl-site.asd file and will be fetched
 automatically by Quicklisp)

## Execution


### From command line, using make:

The Makefile contains various useful commands. 

Among them:

* `make` - to generate the site.
* `make serve` - to generate the site and start up a default test server. 
* `make clean` - to remove the generated files from output directory


### From Lisp 

(The following assumes you have a reasonably recent Quicklisp loaded)

* To generate the static pages:

     (load ".../cl-site/build.lisp") ;; replace with your actual path to cl-site/

  This will load the site-generating code and generate the static pages into `output/`


* To generate the static pages and start the test server:

     (load ".../cl-site/test-serve.lisp") ;; replace with your actual path to cl-site/

 This will start serving the site. The toplevel URL should be printed to the console.

### Known Problems

 At least one of the test servers crashes on load with `invalid number
 of arguments` on certain 1.3.1 releases of SBCL (apparently a problem
 with the `ironclad` library from quicklisp with this sbcl
 version). If you get this error, feel free to [report the
 issue](https://gitlab.common-lisp.net/clo/cl-site/issues), but you
 might also try upgrading your SBCL or try [another
 implementation](https://common-lisp.net/implementations).

