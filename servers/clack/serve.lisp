(in-package :cl-user)

(uiop:with-current-directory (*load-truename*)
  (load "../../build"))

(ql:quickload :cl-site-clack)

;;
;; Start aserve, publish site directory & pages, set default package.
;;
(cl-site-clack:start-server)
(cl-site-clack:publish-cl-site)
(setq *package* (find-package :cl-site))

(format t "

This message from cl-site/servers/clack/serve.lisp should report the url & port 
that the user may visit to browse the site.

")

