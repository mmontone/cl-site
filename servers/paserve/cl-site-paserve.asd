;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-user)

(asdf:defsystem cl-site-paserve
  :name "cl-site-paserve"
  :version "0.0.1"
  :maintainer "Common-Lisp.Net maintainers"
  :author "Dave Cooper & Common-Lisp.Net maintainers"
  :licence "TBD"
  :description "Test server for common-lisp.net for Legacy Portable AllegroServe"
  :depends-on (:cl-site :aserve #+allegro :bordeaux-threads) ;; FLAG system name may be updated to :paserve in future QL.
  :serial t
  :components ((:file "package")
	       (:file "publish")))

